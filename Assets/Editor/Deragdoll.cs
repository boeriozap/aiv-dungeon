﻿using UnityEditor;
using UnityEngine;

public class Deragdoll : EditorWindow
{
	private GameObject go;

	[MenuItem( "Window/Deragdoll" )]
	public static void OpenWindow()
	{
		EditorWindow.GetWindow<Deragdoll>();
	}

	private void OnGUI()
	{
		this.go = EditorGUILayout.ObjectField( this.go, typeof( GameObject ) ) as GameObject;

		if ( GUILayout.Button( "Deragdollise!" ) && this.go != null )
		{
			this.Deragdollise( this.go );
		}
	}

	private void Deragdollise( GameObject go )
	{
		//Cleaning
		foreach ( var j in go.GetComponents<CharacterJoint>() )
		{
			Object.DestroyImmediate( j );
		}

		foreach ( var j in go.GetComponents<Rigidbody>() )
		{
			Object.DestroyImmediate( j );
		}

		foreach ( var j in go.GetComponents<Collider>() )
		{
			Object.DestroyImmediate( j );
		}

		//Cleaning children
		foreach ( Transform child in go.transform )
		{
			this.Deragdollise( child.gameObject );
		}
	}
}