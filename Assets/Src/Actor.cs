﻿using UnityEngine;

public class Actor : MonoBehaviour
{
	public NavMeshAgent Agent { get; private set; }

	public Animator Animator { get; private set; }

	public delegate void EnableHandler( Actor sender, bool enabled );

	public event EnableHandler Enable;

	public bool IsHidden { get; private set; }

	public GameObject HidingPlace { get; private set; }

	private NoiseEmitter[] NoiseEmitters;

	public void Hide( GameObject hidingPlace )
	{
		this.HidingPlace = hidingPlace;
		this.IsHidden = hidingPlace != null;
		this.gameObject.SetActive( !this.IsHidden );
		// FX
	}

	public void MakeNoise( SoundClip soundClip, float intensity )
	{
		NoiseEmitter emitter = System.Array.Find( this.NoiseEmitters, e => e.SoundClip == soundClip );

		if ( emitter == null || intensity <= 0f )
			return;

		emitter.Emit( this );
	}

	protected virtual void Awake()
	{
		this.Agent = this.GetComponent<NavMeshAgent>();
		this.Animator = this.GetComponent<Animator>();
		this.NoiseEmitters = this.GetComponentsInChildren<NoiseEmitter>();
	}

	protected virtual void OnEnable()
	{
		if ( this.Enable != null )
			this.Enable( this, true );
	}

	protected virtual void OnDisable()
	{
		if ( this.Enable != null )
			this.Enable( this, false );
	}
}