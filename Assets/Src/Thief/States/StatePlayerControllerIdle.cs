﻿using UnityEngine;
using System.Collections;

public class StatePlayerControllerIdle : StateMachineBehaviour 
{
	PlayerController controller;


	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		this.controller = animator.GetComponent<PlayerController>();
	}

	
	override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !GameDirector.Instance.MenuIsOpen )
			this.controller.ClickMove();
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{

	}
}
