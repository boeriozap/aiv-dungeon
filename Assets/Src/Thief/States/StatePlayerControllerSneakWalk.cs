﻿using UnityEngine;
using System.Collections;

public class StatePlayerControllerSneakWalk : StateMachineBehaviour
{


	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		
	}


	override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !PlayerController.Instance.CurrentDestination.HasValue )
			return;

		if ( !GameDirector.Instance.MenuIsOpen )
			PlayerController.Instance.ClickMove();

		if ( Vector3.Distance( animator.gameObject.transform.position, PlayerController.Instance.CurrentDestination.Value ) <= PlayerController.Instance.Agent.stoppingDistance )
		{
			animator.SetFloat( "Speed", 0.0f );
			animator.SetFloat( "Stealth", 0.0f );
		}
	}


	override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{

	}


}
