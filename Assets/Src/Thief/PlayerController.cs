﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent( typeof( Animator ) )]
[RequireComponent( typeof( NavMeshAgent ) )]
public class PlayerController : Actor
{
	public static PlayerController Instance { get; private set; }

	private int health;

	public int Health
	{
		get { return this.health; }
		set
		{
			if ( value <= 0 )
			{
				value = 0;

				if ( this.IsHidden )
					this.Hide( null );

				this.collider.enabled = false;
				this.Animator.enabled = false;

				if ( this.CurrentInteraction != null )
				{
					this.CurrentInteraction.Cancel();
					this.CurrentInteraction = null;
				}
			}

			this.health = value;
			this.Animator.SetInteger( "Health", value );
		}
	}

	public float RunSpeed = 4f;
	public float StealthSpeed = 1.5f;
	public float WalkSpeed = 1.15f;
	public Vector3? CurrentDestination { get; set; }
	public Interaction CurrentInteraction;

	private new Collider collider;
	private List<Collider> ragdollColliders = new List<Collider>();
	private float currentStealth;
	private float currentSpeed;
	private int weaponsLayer;

	protected override void Awake()
	{
		base.Awake();

		if ( PlayerController.Instance != null )
			throw new System.Exception( "Duplicate Player Controller Detected" );

		PlayerController.Instance = this;

		this.weaponsLayer = LayerMask.NameToLayer( "Weapons" );

		this.collider = this.GetComponent<Collider>();
		this.ragdollColliders = this.GetComponentsInChildren<Collider>().ToList();
		this.ragdollColliders.Remove( this.collider );
		this.ragdollColliders.ForEach( c => { Physics.IgnoreCollision( c, this.collider, true ); } );

		Interaction.ExecutionEnd += this.OnInteractionEnd;
	}

	private void OnInteractionEnd( Interaction interaction )
	{
		if ( this.CurrentInteraction == interaction )
		{
			this.CurrentInteraction = null;
		}
	}

	internal void ClickMove()
	{
		if ( !GameDirector.Instance.UIInteractionPanel.IsOpen && Input.GetKeyDown( KeyCode.Mouse0 ) )
		{
			Ray vRay = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit vResult;

			if ( Physics.Raycast( vRay, out vResult ) )
			{
				if ( this.CurrentInteraction != null && this.CurrentInteraction.IsCancellable )
				{
					this.CurrentInteraction.Cancel();
					this.CurrentInteraction = null;
				}
				else
				{
					this.Agent.speed = this.WalkSpeed;

					if ( Input.GetKey( KeyCode.LeftControl ) )
					{
						this.currentStealth = 1.0f;
						this.Agent.speed = this.StealthSpeed;
					}
					else
					{
						this.currentStealth = 0.1f;
					}

					if ( Input.GetKey( KeyCode.LeftShift ) )
					{
						this.currentSpeed = 1.0f;
						this.Agent.speed = this.RunSpeed;
					}
					else
					{
						this.currentSpeed = 0.1f;
					}

					this.Animator.SetFloat( "Speed", this.currentSpeed );
					this.Animator.SetFloat( "Stealth", this.currentStealth );

					//TODO: filtrare i collider e rilevare solo quelli del pavimento
					this.CurrentDestination = vResult.point;
					this.Agent.SetDestination( vResult.point );
				}
			}
		}
	}

	public void OnTriggerEnter( Collider other )
	{
		if ( other.gameObject.layer == this.weaponsLayer )
		{
			var weapon = other.GetComponent<Weapon>();

			this.Health -= weapon.Damage;
		}
	}
}