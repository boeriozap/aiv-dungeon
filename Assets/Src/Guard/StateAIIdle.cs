﻿using UnityEngine;
using System.Collections;
using System.Linq;
public class StateAIIdle : StateMachineBehaviour
{
	//TODO: trovare un meccanismo di inizializzazione intelligente
	private AIController m_hOwner;
	private float m_fCurrentTime;

	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		this.m_hOwner = animator.gameObject.GetComponent<AIController>();

		this.m_hOwner.Sight.RaycastCheck = true;

		this.m_fCurrentTime = Random.Range( 0f, m_hOwner.IdleTime );
	}

	override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !animator.IsInTransition( layerIndex ) )
		{
			// setting destination
			if ( m_fCurrentTime <= 0f && !m_hOwner.Target )
			{
				bool bSuccess = this.m_hOwner.GoToRandomDestination(); //può provocare il cambiamento di stato (idealmente sempre)
				if ( !bSuccess )
					m_fCurrentTime = this.m_hOwner.IdleTime;
			}
			// wait...
			else
			{
				this.m_fCurrentTime -= Time.deltaTime;
			}
		}
	}

	override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{

	}
}
