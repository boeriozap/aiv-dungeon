﻿using UnityEngine;

public class StateAIChasing : StateMachineBehaviour
{
	public float AttackDistance = 1f;
	public float AttackRotationTolerance = 10f;
	public float RotationSpeed = 500f;

	private AIController m_hOwner;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		m_hOwner = animator.GetComponent<AIController>();

		m_hOwner.Sight.RaycastCheck = false;

		//Debug.Log( "Angry Enter();" );
	}

	public override void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !animator.IsInTransition( layerIndex ) )
		{
			m_hOwner.GoToDestination();

			if ( m_hOwner.Target != null && Vector3.Distance( m_hOwner.Target.transform.position, m_hOwner.transform.position ) <= AttackDistance )
			{
				m_hOwner.Agent.Stop();
				animator.SetFloat( "Speed", 0f );

				var direction = ( m_hOwner.Target.transform.position - m_hOwner.transform.position ).normalized;
				var angle = Vector3.Angle( direction, this.m_hOwner.transform.forward );

				if ( angle > this.AttackRotationTolerance )
				{
					this.LookAtTarget();
				}
				else
				{
					int AttackID = Random.Range( 1, 4 );
					animator.SetInteger( "AttackID", AttackID );
				}
			}
			else
			{
				m_hOwner.Agent.Resume();
			}

			//Debug.Log( "Angry Update();" );
		}
	}

	public override void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		m_hOwner.Sight.RaycastCheck = true;
		//Debug.Log( "Angry Exit();" );
	}

	private void LookAtTarget()
	{
		var distance = m_hOwner.Target.transform.position - m_hOwner.transform.position;
		float targetAngle = Mathf.Repeat( 90f - ( Mathf.Atan2( distance.z, distance.x ) * Mathf.Rad2Deg ), 360f );
		float currentAngle = this.m_hOwner.transform.rotation.eulerAngles.y;

		if ( !( currentAngle <= targetAngle + 0.1f && currentAngle >= targetAngle - 0.1f ) )
		{
			if ( Mathf.Abs( currentAngle - targetAngle ) > 180f )
			{
				if ( currentAngle < targetAngle )
					currentAngle += 360f;
				else
					targetAngle += 360f;
			}

			float targetRotation = targetAngle - currentAngle;
			float newRotation = Mathf.Sign( targetRotation ) * RotationSpeed * Time.deltaTime;

			float newAngle = currentAngle + newRotation;

			//Dobbiamo fare il seguente controllo perché altrimenti, se la rotazione è troppo grande, non entreremo mai nel Range di tolleranza
			if ( ( currentAngle < targetAngle && newAngle > targetAngle ) ||
				( currentAngle > targetAngle && newAngle < targetAngle ) )
			{
				newRotation = targetRotation;
			}

			this.m_hOwner.transform.Rotate( 0f, newRotation, 0f );
		}
	}
}