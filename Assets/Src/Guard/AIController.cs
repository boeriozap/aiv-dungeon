﻿using SWS;
using UnityEngine;

[RequireComponent( typeof( NavMeshAgent ) )]
[RequireComponent( typeof( Animator ) )]
public class AIController : Actor
{
	public GameObject Target;

	public Weapon Weapon;

	public Sight Sight { get; private set; }
	public Hearing Hearing { get; private set; }

	public float IdleTime = 20f;
	public float WalkSpeed = 0.4f;
	public float RunSpeed = 0.8f;

	public PathFollowerData PathFollowerData;

	private Vector3 targetLastPosition;

	protected override void Awake()
	{
		base.Awake();

		this.Sight = this.gameObject.GetComponentInChildren<Sight>();
		this.Hearing = this.gameObject.GetComponentInChildren<Hearing>();
		this.Weapon = this.GetComponentInChildren<Weapon>();
	}

	protected override void OnEnable()
	{
		base.OnEnable();

		this.Sight.enabled = true;

		NoiseEmitter.NoiseEmission += this.OnNoiseEmission;
	}

	protected override void OnDisable()
	{
		base.OnDisable();

		this.Sight.enabled = false;

		NoiseEmitter.NoiseEmission -= this.OnNoiseEmission;
	}

	private void Start()
	{
		//HACK: Only for debug
		///this.Animator.SetBool("Rage", true);
	}

	internal bool GoToRandomDestination()
	{
		//TODO: Gestire il caso in cui ritorna null
		this.Target = GameDirector.Instance.GetWaypoint(); //Rimuove da una lista un gameobject
		if ( this.Target == null )
			return false;

		this.Agent.speed = this.WalkSpeed;
		this.Agent.SetDestination( this.Target.transform.position );

		this.Animator.SetFloat( "Speed", this.WalkSpeed );
		return true;
	}

	public void ActorSighted( Actor target )
	{
		target.Enable += this.OnTargetEnable;
		this.Target = target.gameObject;
		this.targetLastPosition = target.transform.position;

		//TODO: find a way to listen (set Alerted=true) the player
		this.Animator.SetBool( "Rage", true );

		//this.Animator.SetBool( "Alerted", true );
		this.GoToDestination();
	}

	private void OnTargetEnable( Actor sender, bool enabled )
	{
		this.Target = sender.IsHidden ? PlayerController.Instance.HidingPlace : sender.gameObject;
	}

	public void GoToDestination()
	{
		if ( this.Target != null )
			this.targetLastPosition = this.Target.transform.position;

		this.Agent.SetDestination( this.targetLastPosition );
		this.Animator.SetFloat( "Speed", this.WalkSpeed );
		this.Agent.speed = this.WalkSpeed;
	}

	public void ActorEscaped()
	{
		this.Animator.SetBool( "Rage", false );
		Actor actor = this.Target.GetComponent<Actor>();
		if ( actor != null )
			actor.Enable -= this.OnTargetEnable;
		this.Target = null;
		this.Agent.SetDestination( this.targetLastPosition );
		this.Animator.SetFloat( "Speed", this.WalkSpeed );
		this.Agent.speed = this.WalkSpeed;
		Debug.Log( "Target lost for " + this.gameObject.name + ": " + this.Target );
	}

	private void OnNoiseEmission( Actor source, NoiseEmitter emitter )
	{
		if ( source != PlayerController.Instance )
			return;

		var emitterPos = emitter.transform.position;
		var noiseNearestPoint = ( this.transform.position - emitterPos ).normalized * emitter.Intensity;

		if ( Vector3.Distance( this.transform.position, emitterPos + noiseNearestPoint ) >
			 this.Hearing.Distance )
			return;

		NavMeshPath path = new NavMeshPath();
		if ( NavMesh.CalculatePath( this.Agent.nextPosition, emitterPos, -1, path ) )
		{
			Vector3[] corners = path.corners;
			float distance = 0f;
			for ( int i = 1; i < corners.Length; i++ )
			{
				distance += Vector3.Distance( corners[i], corners[i - 1] );
			}

			if ( distance - emitter.Intensity <= this.Hearing.Distance )
			{
				// The AI hears the player
				this.ActorSighted( PlayerController.Instance );
			}
		}
	}
}