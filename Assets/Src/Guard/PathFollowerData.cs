﻿using SWS;
using UnityEngine;

public class PathFollowerData : MonoBehaviour
{
	public enum LoopType { Loop, PingPong }

	public LoopType Type = LoopType.Loop;

	public float DistanceTolerance = 0.1f;

	private bool pingPongForwardDirection = true;

	public Transform TargetWaypoint
	{
		get { return targetWaypoint; }
		set
		{
			if ( value != null )
			{
				for ( int i = 0; i < this.Path.waypoints.Length; i++ )
				{
					if ( value == this.Path.waypoints[i] )
					{
						this.targetWaypoint = value;
						this.targetWaypointIndex = i;
						return;
					}
				}
				Debug.LogError( "Cannot set the target waypoint: it doesn't belong to the current path" );
			}

			this.targetWaypoint = null;
		}
	}

	private Transform targetWaypoint;
	private int targetWaypointIndex;
	public PathManager Path;

	public Transform GetNearestWaypoint()
	{
		float minDistance = float.MaxValue;
		float distance;
		int nearest = 0;

		for ( int i = 0; i < Path.waypoints.Length; i++ )
		{
			distance = ( Path.waypoints[i].position - transform.position ).magnitude;
			if ( distance < minDistance )
			{
				nearest = i;
				minDistance = distance;
			}
		}
		return Path.waypoints[nearest];
	}

	public void SetNextWaypoint()
	{
		if ( this.TargetWaypoint == null )
		{
			this.TargetWaypoint = this.GetNearestWaypoint();
		}
		else
		{
			float distance = ( Path.waypoints[this.targetWaypointIndex].position - transform.position ).magnitude;
			if ( distance < this.DistanceTolerance )
			{
				switch ( this.Type )
				{
					case LoopType.Loop:
					this.TargetWaypoint = this.Path.waypoints[( this.targetWaypointIndex + 1 ) % this.Path.waypoints.Length];
					break;

					case LoopType.PingPong:
					var index = this.targetWaypointIndex + ( this.pingPongForwardDirection ? 1 : -1 );

					if ( index >= this.Path.waypoints.Length )
					{
						this.pingPongForwardDirection = false;
						index = this.Path.waypoints.Length - 1;
					}
					else if ( index < 0 )
					{
						this.pingPongForwardDirection = true;
						index = 0;
					}

					this.TargetWaypoint = this.Path.waypoints[index];
					break;

					default:
					break;
				}
			}
		}
	}

	public void Pause()
	{
	}

	public void Resume()
	{
	}

	public void Stop()
	{
	}
}