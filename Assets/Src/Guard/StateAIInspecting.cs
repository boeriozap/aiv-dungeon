﻿using UnityEngine;

public class StateAIInspecting : StateMachineBehaviour
{
	private AIController owner;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		owner = animator.GetComponent<AIController>();
	}

	override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !animator.IsInTransition( layerIndex ) )
		{
			// If is approximately to the destination
			if ( owner.Agent.remainingDistance <= PlayerController.Instance.Agent.stoppingDistance )
			{
				owner.Target = null;
				animator.SetFloat( "Speed", 0.0f );
			}
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
	}
}