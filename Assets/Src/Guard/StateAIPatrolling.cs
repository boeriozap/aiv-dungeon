﻿using UnityEngine;

public class StateAIPatrolling : StateMachineBehaviour
{
	private AIController owner;
	private float currentTime;

	override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		owner = animator.GetComponent<AIController>();
	}

	override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( !animator.IsInTransition( layerIndex ) )
		{
			// setting destination
			if ( this.owner.PathFollowerData.Path != null )
			{
				// Patrolling the path
				this.owner.PathFollowerData.SetNextWaypoint();
				this.owner.Target = this.owner.PathFollowerData.TargetWaypoint.gameObject;
				this.owner.GoToDestination();
			}
			else
			{
				// Patrolling random path
				if ( this.currentTime <= 0f && !this.owner.Target )
				{
					bool bSuccess = this.owner.GoToRandomDestination(); //può provocare il cambiamento di stato (idealmente sempre)
					if ( !bSuccess )
						this.currentTime = this.owner.IdleTime;
				}
				else
				{
					this.currentTime -= Time.deltaTime;
				}
			}
			// wait...
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		this.owner.PathFollowerData.TargetWaypoint = null;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}