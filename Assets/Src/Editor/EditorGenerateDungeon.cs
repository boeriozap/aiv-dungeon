﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using AIV.Algorithms.MazeGeneration;

public class EditorGenerateDungeon : EditorWindow
{
	public float CellSize = 0.514f;
	public float WallHeight = 1.2f;
	public int Seed = 5;
	public int Rows = 21;
	public int Cols = 21;
	public int Rooms = 50;
	public int RoomSizeMin = 3;
	public int RoomSizeMax = 5;
	public bool RemoveDeadEnds = true;
	public float WallLitChance = 0.3f;

	public Object m_hFloorResource;
	public Object m_hDirtResource;
	public Object m_hPillarResource;
	public Object m_hWallResource;
	public Object m_hArchResource;
	public Object m_hWallLithResource;

	private static EditorGenerateDungeon m_hGUI;

	[MenuItem( "Dungeon Generator/Generate" )]
	public static void Generate()
	{
		m_hGUI = EditorGenerateDungeon.GetWindow<EditorGenerateDungeon>();
		m_hGUI.Show();
	}

	[MenuItem( "Dungeon Generator/Clear" )]
	public static void Clear()
	{
		GameObject hDungeon = GameObject.Find( "Dungeon" );
		GameObject.DestroyImmediate( hDungeon );
	}

	private void OnGUI()
	{
		CellSize = EditorGUILayout.FloatField( "Cell Size", CellSize );
		WallHeight = EditorGUILayout.FloatField( "Wall Height", WallHeight );
		Seed = EditorGUILayout.IntField( "Seed", Seed );
		Rows = EditorGUILayout.IntField( "Rows", Rows );
		Cols = EditorGUILayout.IntField( "Cols", Cols );
		Rooms = EditorGUILayout.IntField( "Rooms", Rooms );
		RoomSizeMin = EditorGUILayout.IntField( "Minimum Room Size", RoomSizeMin );
		RoomSizeMax = EditorGUILayout.IntField( "Maximum Room Size", RoomSizeMax );
		RemoveDeadEnds = EditorGUILayout.Toggle( "Remove Dead Ends", RemoveDeadEnds );
		WallLitChance = EditorGUILayout.FloatField( "Wall Lit Chance", WallLitChance );

		if ( GUILayout.Button( "Generate" ) )
		{
			m_hFloorResource = Resources.Load( "Dungeon/Floor" );
			m_hDirtResource = Resources.Load( "Dungeon/Dirt" );
			m_hPillarResource = Resources.Load( "Dungeon/Pillar" );
			m_hWallResource = Resources.Load( "Dungeon/Wall" );
			m_hArchResource = Resources.Load( "Dungeon/Arch" );
			m_hWallLithResource = Resources.Load( "Dungeon/WallLit" );

			m_hGUI.Close();

			//Creazione della struttura
			Dungeon hDung = new Dungeon( Seed, Rows, Cols, Rooms, RoomSizeMin, RoomSizeMax, RemoveDeadEnds );

			//Creazione del modello
			GameObject hParent = new GameObject( "Dungeon" );
			this.BuildTileset( hDung, hParent );
		}
	}

	private GameObject InstantiateWall()
	{
		Object hWallResource = Random.Range( 0f, 1f ) <= WallLitChance ? m_hWallLithResource : m_hWallResource;
		return GameObject.Instantiate( hWallResource ) as GameObject;
	}

	private void BuildTileset( Dungeon Instance, GameObject hParent )
	{
		GameObject hDungeonContainer = hParent;
		hDungeonContainer.name = "Dungeon";

		for ( int i = 0; i < Instance.Tiles.GetLength( 0 ); i++ )
		{
			for ( int j = 0; j < Instance.Tiles.GetLength( 1 ); j++ )
			{
				Tile hCurrent = Instance.Tiles[i, j];
				GameObject hTileModel;

				if ( hCurrent.Type == ContentType.Floor || hCurrent.Type == ContentType.Corridor ||
					hCurrent.Type == ContentType.Door )
				{
					hTileModel = GameObject.Instantiate( m_hFloorResource ) as GameObject;

					if ( hCurrent.Type == ContentType.Floor )
						hTileModel.tag = "Floor";
					else if ( hCurrent.Type == ContentType.Corridor )
						hTileModel.tag = "Corridor";
					else
						hTileModel.tag = "Door";

					Vector3 vPosition = new Vector3( (float) hCurrent.X + CellSize * hCurrent.X, 0,
													(float) hCurrent.Y + CellSize * hCurrent.Y );
					hTileModel.transform.position = vPosition;
					hTileModel.transform.parent = hDungeonContainer.transform;

					Tile hNorthern = hCurrent.Neighbours[(int) Direction.North];
					Tile hSouthern = hCurrent.Neighbours[(int) Direction.South];
					Tile hEastern = hCurrent.Neighbours[(int) Direction.East];
					Tile hWestern = hCurrent.Neighbours[(int) Direction.West];

					if ( hNorthern.Type == ContentType.Wall || hNorthern.Type == ContentType.Filled )
					{
						GameObject hWall = this.InstantiateWall();
						Vector3 vWallPosition = vPosition + new Vector3( 0f, 0f, -CellSize * 1.5f );
						hWall.transform.position = vWallPosition;
						hWall.transform.Rotate( 0f, 0f, 180f );
						hWall.transform.parent = hDungeonContainer.transform;

						GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
						hPillar.transform.position = vWallPosition + new Vector3( -CellSize * 1.5f, 0, 0 );
						hPillar.transform.parent = hDungeonContainer.transform;
					}

					if ( hSouthern.Type == ContentType.Wall || hSouthern.Type == ContentType.Filled )
					{
						GameObject hWall = this.InstantiateWall();
						Vector3 vWallPosition = vPosition + new Vector3( 0f, 0f, CellSize * 1.5f );
						hWall.transform.position = vWallPosition;
						hWall.transform.parent = hDungeonContainer.transform;

						GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
						hPillar.transform.position = vWallPosition + new Vector3( CellSize * 1.5f, 0, 0 );
						hPillar.transform.parent = hDungeonContainer.transform;
					}

					if ( hEastern.Type == ContentType.Wall || hEastern.Type == ContentType.Filled )
					{
						GameObject hWall = this.InstantiateWall();
						Vector3 vWallPosition = vPosition + new Vector3( CellSize * 1.5f, 0f, 0f );
						hWall.transform.position = vWallPosition;
						hWall.transform.Rotate( 0f, 0f, 90f );
						hWall.transform.parent = hDungeonContainer.transform;

						GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
						hPillar.transform.position = vWallPosition + new Vector3( 0, 0, -CellSize * 1.5f );
						hPillar.transform.parent = hDungeonContainer.transform;
					}

					if ( hWestern.Type == ContentType.Wall || hWestern.Type == ContentType.Filled )
					{
						GameObject hWall = this.InstantiateWall();
						Vector3 vWallPosition = vPosition + new Vector3( -CellSize * 1.5f, 0f, 0f );
						hWall.transform.position = vWallPosition;
						hWall.transform.Rotate( 0f, 0f, -90f );
						hWall.transform.parent = hDungeonContainer.transform;

						GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
						hPillar.transform.position = vWallPosition + new Vector3( 0, 0, CellSize * 1.5f );
						hPillar.transform.parent = hDungeonContainer.transform;
					}

					if ( hCurrent.Type == ContentType.Door )
					{
						if ( hNorthern.Type == ContentType.Floor )
						{
							GameObject hDoor = GameObject.Instantiate( m_hArchResource ) as GameObject;
							Vector3 vWallPosition = vPosition + new Vector3( 0f, 0f, -CellSize * 1.5f );
							hDoor.transform.position = vWallPosition;
							hDoor.transform.Rotate( 0f, 0f, 90f );
							hDoor.transform.parent = hDungeonContainer.transform;

							if ( hEastern.Type == ContentType.Door )
							{
								GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
								hPillar.transform.position = vWallPosition + new Vector3( CellSize * 1.5f, 0, 0 );
								hPillar.transform.parent = hDungeonContainer.transform;
							}
						}

						if ( hSouthern.Type == ContentType.Floor )
						{
							GameObject hDoor = GameObject.Instantiate( m_hArchResource ) as GameObject;
							Vector3 vWallPosition = vPosition + new Vector3( 0f, 0f, CellSize * 1.5f );
							hDoor.transform.position = vWallPosition;
							hDoor.transform.Rotate( 0f, 0f, -90f );
							hDoor.transform.parent = hDungeonContainer.transform;

							if ( hWestern.Type == ContentType.Door )
							{
								GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
								hPillar.transform.position = vWallPosition + new Vector3( -CellSize * 1.5f, 0, 0 );
								hPillar.transform.parent = hDungeonContainer.transform;
							}
						}

						if ( hEastern.Type == ContentType.Floor )
						{
							GameObject hDoor = GameObject.Instantiate( m_hArchResource ) as GameObject;
							Vector3 vWallPosition = vPosition + new Vector3( CellSize * 1.5f, 0f, 0f );
							hDoor.transform.position = vWallPosition;
							hDoor.transform.parent = hDungeonContainer.transform;

							if ( hSouthern.Type == ContentType.Door )
							{
								GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
								hPillar.transform.position = vWallPosition + new Vector3( 0, 0, CellSize * 1.5f );
								hPillar.transform.parent = hDungeonContainer.transform;
							}
						}

						if ( hWestern.Type == ContentType.Floor )
						{
							GameObject hDoor = GameObject.Instantiate( m_hArchResource ) as GameObject;
							Vector3 vWallPosition = vPosition + new Vector3( -CellSize * 1.5f, 0f, 0f );
							hDoor.transform.position = vWallPosition;
							hDoor.transform.parent = hDungeonContainer.transform;

							if ( hNorthern.Type == ContentType.Door )
							{
								GameObject hPillar = GameObject.Instantiate( m_hPillarResource ) as GameObject;
								hPillar.transform.position = vWallPosition + new Vector3( 0, 0, -CellSize * 1.5f );
								hPillar.transform.parent = hDungeonContainer.transform;
							}
						}
					}

					//if ( hCurrent.Type == ContentType.Floor )
					//{
					//	if ( Random.Range( 0f, 1f ) <= FurnitureChance )
					//	{
					//		GameObject hFurniture = GameObject.Instantiate( FurnitureList[Random.Range( 0, FurnitureList.Count )] ) as GameObject;
					//		hFurniture.transform.position = vPosition;
					//		hFurniture.transform.Rotate( 0f, Random.Range( 0f, 90f ), 0f );
					//		hFurniture.transform.parent = hDungeonContainer.transform;
					//	}
					//}
				}
				else if ( hCurrent.Type == ContentType.Filled || hCurrent.Type == ContentType.Wall )
				{
					hTileModel = GameObject.Instantiate( m_hDirtResource ) as GameObject;

					Vector3 vPosition = new Vector3( (float) hCurrent.X + CellSize * hCurrent.X, WallHeight,
													(float) hCurrent.Y + CellSize * hCurrent.Y );
					hTileModel.transform.position = vPosition;
					hTileModel.transform.parent = hDungeonContainer.transform;
				}
			}
		}
	}
}