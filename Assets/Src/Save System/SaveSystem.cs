﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using System.IO;

public class SaveSystem
{
	#region Singleton
	public static SaveSystem Instance { get; private set; }

	static SaveSystem()
	{
		Instance = new SaveSystem();
	}

	#endregion
	
	private const int m_iSlots = 10;


	//Questo metodo serve a popolare la GUI con i nomi dei file da salvare
	public string[] LoadFileNames()
	{
		string[] hRes = new string[m_iSlots];

		for ( int i = 0; i < m_iSlots; i++ )
		{
			hRes[i] = PlayerPrefs.GetString( "__SaveGame" + i + "RecordName" );
		}

		return hRes;
	}

	//Metodo per il salvataggio effettivo di un record
	public void Save(int iSlot, SaveGame hSaveGame)
	{
        string sEncodedSaveGame = hSaveGame.ToXml();

		//Salviamo l'oggetto effettivo nel DB di sistema
		PlayerPrefs.SetString( "__SaveGame" + iSlot, sEncodedSaveGame );

		//Salviamo il nome del record nel db di sistema
		PlayerPrefs.SetString( "__SaveGame" + iSlot + "RecordName", hSaveGame.Name );

		//Salvataggio effettivo
		PlayerPrefs.Save();
	}


	public SaveGame Load( int iSlot )
	{ 		
		string sXmlEncodedData = PlayerPrefs.GetString("__SaveGame" + iSlot);

        return sXmlEncodedData.ToObject<SaveGame>();
	}



	/// <summary>
	/// Questa classe ha il compito di immagazinare tutti i dati mutabili della simulazione
	/// </summary>
	[Serializable]
	public class SaveGame
	{
		public string Name;
		public DateTime Date;

		public int DungeonSeed;
	}




}



