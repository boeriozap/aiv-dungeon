﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using System.IO.Compression;

public static class Extensions
{
    public static string ToXml(this object hInstance)
    {
        XmlSerializer hSerializer = new XmlSerializer(hInstance.GetType());
        StringBuilder hSb = new StringBuilder();

        using (TextWriter hWriter = new StringWriter(hSb))
        {
            hSerializer.Serialize(hWriter, hInstance);
        }

        return hSb.ToString();
    }

   
    public static T ToObject<T>(this string hXmlEncodedObject) where T : class
    {
        XmlSerializer hSerializer = new XmlSerializer(typeof(T));

        using (TextReader hReader = new StringReader(hXmlEncodedObject))
        {
            return hSerializer.Deserialize(hReader) as T;
        }
    }

    public static string Compress(this string sThis)
    {
        byte[] hInputData = System.Text.Encoding.Unicode.GetBytes(sThis);
        byte[] hOutData;

        using (MemoryStream hMs = new MemoryStream(hInputData))
        {
            using (GZipStream hZip = new GZipStream(hMs, CompressionMode.Compress))
            {
                hZip.Write(hInputData, 0, hInputData.Length);
            }

            hOutData = new byte[hMs.Length];
            hMs.Read(hOutData, 0, hOutData.Length);
        }

        return System.Convert.ToBase64String(hOutData);
    }

    public static string Decompress(this string sThis)
    {
        return null; //Todo implementare
    }
}
