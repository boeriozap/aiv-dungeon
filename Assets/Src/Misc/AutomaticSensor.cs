﻿using System;
using UnityEngine;

public class AutomaticSensor : MonoBehaviour
{
	public LayerMask CollisionMask;

	public delegate bool CollisionValidatorHandler( Collider hColl );

	public CollisionValidatorHandler CollisionValidator
	{
		get
		{
			return collisionValidator;
		}
		set
		{
			//Non siamo pazzi, tutto ciò per evitare che vengano aggiunte più callback
			collisionValidator = null;
			collisionValidator = value;
		}
	}

	private CollisionValidatorHandler collisionValidator;

	public event Action<Collider> TriggerEnter;

	public event Action<Collider> TriggerExit;

	public event Action<Collider> TriggerStay;

	private void OnTriggerEnter( Collider hColl )
	{
		if ( TriggerEnter != null )
		{
			if ( ( CollisionValidator != null && CollisionValidator( hColl ) ) ||
				( ( 1 << hColl.gameObject.layer ) & this.CollisionMask ) != 0 )
			{
				TriggerEnter(hColl);
			}
		}
	}

	private void OnTriggerExit( Collider hColl )
	{
		if ( TriggerExit != null )
		{
			if ( ( CollisionValidator != null && CollisionValidator( hColl ) ) ||
				( ( 1 << hColl.gameObject.layer ) & this.CollisionMask ) != 0 )
			{
				TriggerExit( hColl );
			}
		}
	}

	private void OnTriggerStay( Collider hColl )
	{
		if ( TriggerStay != null )
		{
			if ( ( CollisionValidator != null && CollisionValidator( hColl ) ) ||
				( ( 1 << hColl.gameObject.layer ) & this.CollisionMask ) != 0 )
			{
				TriggerStay( hColl );
			}
		}
	}
}