﻿using System;
using UnityEngine;
using System.Collections;

public class NoiseEmitter : MonoBehaviour
{
	public delegate void NoiseEmissionHandler( Actor source, NoiseEmitter emitter );

	public static event NoiseEmissionHandler NoiseEmission;

	public SoundClip SoundClip = SoundClip.None;
	public float Intensity = 1f;

	private AutoDisableAudioClip audioClip;

	public void Emit( Actor source, Vector3 position )
	{
		if ( this.audioClip == null )
			this.audioClip = AudioManager.Instance.Play( this.SoundClip, position );

		this.MakeNoise( source );
	}

	public void Emit( Actor source )
	{
		this.Emit( source, this.transform.position );
	}

	private void MakeNoise( Actor source )
	{
		if ( NoiseEmitter.NoiseEmission != null )
			NoiseEmitter.NoiseEmission( source, this );
	}
}