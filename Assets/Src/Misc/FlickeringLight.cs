﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Light))]
public class FlickeringLight : MonoBehaviour 
{
	private Light m_hLight;

	[Range( 0f, 0.01f )]
	public float Radius = 0.0003f;

	[Range( 0f, 1.5f )]
	public float RadiusChangeRate = 0.08f;

	[Range( 0f, 3f )]
	public float DeltaIntensity = 3f;

	[Range( 0f, 1.5f )]
	public float DeltaRange = 1.5f;

	private float m_fElapsedTime;


	private bool m_bToLerp;
	private Vector3 m_vDestPosition;
	private Vector3 m_vInitPosition;
	private Vector3 m_vCurrentInitLerpPosition;


	void Awake()
	{
		m_hLight = this.GetComponent<Light>();
		this.transform.position += Random.insideUnitSphere * this.Radius * 2f;
		m_vInitPosition = this.transform.position;
		m_vCurrentInitLerpPosition = m_vInitPosition;
		this.m_hLight.intensity += Random.Range( -this.DeltaIntensity, this.DeltaIntensity );
		this.m_hLight.range += Random.Range( -this.DeltaRange, this.DeltaRange );
	}

	void Start()
	{
		
	}

	void Update () 
	{
		if ( m_bToLerp )
		{
			m_fElapsedTime += Time.deltaTime;

			if ( m_fElapsedTime <= RadiusChangeRate )
			{
				this.transform.position = Vector3.Lerp( m_vCurrentInitLerpPosition, m_vDestPosition, m_fElapsedTime / RadiusChangeRate );
			}
			else
			{
				m_bToLerp = false;
			}
		}
		else
		{
			m_vDestPosition				= m_vInitPosition + Random.insideUnitSphere * this.Radius;
			m_vCurrentInitLerpPosition	= this.transform.position;
			m_fElapsedTime				= 0.0f;
			m_bToLerp					= true;
		}
	}
}
