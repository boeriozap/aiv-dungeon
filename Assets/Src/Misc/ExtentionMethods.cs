﻿using UnityEngine;

public static class ExtentionMethods
{
	public static Vector2 XZ( this Vector3 vec )
	{
		return new Vector2( vec.x, vec.z );
	}

	public static void Raise( this System.Action @event )
	{
		if ( @event != null )
			@event();
	}

	public static void Raise<T>( this System.Action<T> @event, T arg1 )
	{
		if ( @event != null )
			@event( arg1 );
	}

	public static void Raise<T1, T2>( this System.Action<T1, T2> @event, T1 arg1, T2 arg2 )
	{
		if ( @event != null )
			@event( arg1, arg2 );
	}
}