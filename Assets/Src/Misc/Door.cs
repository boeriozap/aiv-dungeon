﻿using UnityEngine;

// TODO: Fix trigger enter on closing transition
public class Door : MonoBehaviour
{
	public enum DoorState
	{
		Closed, OpenPositive, OpenNegative,
	}

	public int ActorsInsideTrigger;
	public DoorState OpenedSide = DoorState.Closed;
	public Collider Collider;
	public bool IsLocked;
	public float NoiseIntensity = 2f;

	private AutomaticSensor sensor;
	private Animator animator;
	private NavMeshObstacle navObstacle;
	private readonly int animOpenClose_Pos = Animator.StringToHash( "OpenClose_pos" );
	private readonly int animOpenClose_Neg = Animator.StringToHash( "OpenClose_neg" );

	public void Open( Vector3 openerPosition )
	{
		if ( this.OpenedSide != DoorState.Closed )
			return;

		if ( Vector3.Dot( this.transform.right, this.transform.position - openerPosition ) > 0 )
		{
			this.animator.SetTrigger( animOpenClose_Pos );
			this.OpenedSide = DoorState.OpenPositive;
		}
		else
		{
			this.animator.SetTrigger( animOpenClose_Neg );
			this.OpenedSide = DoorState.OpenNegative;
		}

		if ( this.Collider != null )
			this.Collider.enabled = false;
		this.navObstacle.enabled = false;
	}

	public void Close()
	{
		switch ( this.OpenedSide )
		{
			case DoorState.OpenPositive:
				{
					this.animator.SetTrigger( this.animOpenClose_Pos );
				}
				break;

			case DoorState.OpenNegative:
				{
					this.animator.SetTrigger( this.animOpenClose_Neg );
				}
				break;
		}
		this.OpenedSide = DoorState.Closed;
		this.navObstacle.enabled = true;

		if ( this.Collider != null )
			this.Collider.enabled = true;
	}

	private void Awake()
	{
		this.sensor = GetComponentInChildren<AutomaticSensor>();
		this.animator = GetComponentInChildren<Animator>();
		this.navObstacle = GetComponentInChildren<NavMeshObstacle>();
	}

	private void OnEnable()
	{
		this.sensor.CollisionValidator = this.SensorCollisionValidator;
		this.sensor.TriggerEnter += OnSensorTriggerEnter;
		this.sensor.TriggerExit += OnSensorTriggerExit;
	}

	private void OnDisable()
	{
		this.sensor.CollisionValidator = null;
		this.sensor.TriggerEnter -= OnSensorTriggerEnter;
		this.sensor.TriggerExit -= OnSensorTriggerExit;
	}

	private void OnSensorTriggerEnter( Collider hColl )
	{
		if ( this.ActorsInsideTrigger == 0 )
		{
			this.Open( hColl.transform.position );
		}

		++this.ActorsInsideTrigger;
	}

	private void OnSensorTriggerExit( Collider coll )
	{
		--this.ActorsInsideTrigger;
		if ( this.ActorsInsideTrigger == 0 )
		{
			this.Close();
		}
	}

	private bool SensorCollisionValidator( Collider hColl )
	{
		if ( hColl.gameObject.GetComponent<AIController>() != null )
			return true;
		return false;
	}
}