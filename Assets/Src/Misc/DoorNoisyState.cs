﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DoorNoisyState : StateMachineBehaviour
{
	public SoundClip SoundClip = SoundClip.None;
	private List<NoiseEmitter> noiseEmitters;
	private Actor opener;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	public override void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		this.noiseEmitters = animator.GetComponents<NoiseEmitter>().ToList();
		this.opener = animator.GetComponent<InteractableObject>().LastInteractor;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	public override void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
	{
		if ( this.noiseEmitters.Count > 0 )
		{
			if ( this.SoundClip == SoundClip.None )
			{
				this.noiseEmitters[0].Emit( this.opener );
			}
			else
			{
				var emitter = this.noiseEmitters.Find( e => e.SoundClip == this.SoundClip );
				if ( emitter != null )
					emitter.Emit(this.opener);
			}
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}