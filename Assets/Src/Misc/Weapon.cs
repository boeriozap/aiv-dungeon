﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
	public int Damage = 100;
	public new Collider collider;

	void Start()
	{
		this.collider.enabled = false;
	}

	public virtual void StartSwing()
	{
		this.collider.enabled = true;
	}

	public virtual void EndSwing()
	{
		this.collider.enabled = false;
	}
}