﻿using UnityEngine;

[RequireComponent( typeof( SphereCollider ) )]
public class Sight : MonoBehaviour
{
	public float Angle = 90f;
	public float RaycastPeriod = 0.2f;

	private bool playerWasSeen = false;

	// Layer presi in considerazione per il rilevamento di attori
	public LayerMask TakenIntoAccount;

	// Layer ignorati dai raycast per potere vedere attraverso alcuni props
	public LayerMask SeeThrough;

	public bool RaycastCheck = true;

	public new SphereCollider collider { get; private set; }

	private AIController aiController;
	private float raycastTimer = 0f;

	private void Awake()
	{
		this.collider = this.GetComponent<SphereCollider>();
		this.aiController = this.GetComponentInParent<AIController>();
	}

	private void FixedUpdate()
	{
		this.raycastTimer += Time.fixedDeltaTime;
	}

	private void OnTriggerStay( Collider coll )
	{
		if ( ( this.TakenIntoAccount & 1 << coll.gameObject.layer ) != 0 )
		{
			if ( this.raycastTimer >= this.RaycastPeriod && this.RaycastCheck )
			{
				CapsuleCollider capColl = coll as CapsuleCollider;
				if ( capColl == null )
					throw new System.NotImplementedException( "Can't find capsule collider" );

				Vector3 targetPosition = coll.transform.position;
				targetPosition.y = this.transform.position.y;

				this.raycastTimer = 0f;

				//Vector3 delta = coll.transform.position - this.transform.position;
				//float actualAngle = Mathf.Atan2( delta.x, delta.z ) * Mathf.Rad2Deg;

				float halfAngle = this.Angle * 0.5f;

				Vector3 fovPointLeft = Quaternion.Euler( 0f, -halfAngle, 0f ) * this.transform.forward;
				Vector3 fovPointRight = Quaternion.Euler( 0f, halfAngle, 0f ) * this.transform.forward;

				Debug.DrawLine( this.transform.position, this.transform.position + fovPointLeft, Color.yellow );
				Debug.DrawLine( this.transform.position, this.transform.position + fovPointRight, Color.yellow );

				float fovAngleLeft = Mathf.Repeat( Mathf.Atan2( fovPointLeft.z, fovPointLeft.x ), 2f * Mathf.PI ) * Mathf.Rad2Deg;
				float fovAngleRight = Mathf.Repeat( Mathf.Atan2( fovPointRight.z, fovPointRight.x ), 2f * Mathf.PI ) * Mathf.Rad2Deg;

				Vector3 distance = ( targetPosition - this.transform.position ).normalized;
				Vector3 ortho = new Vector3( -distance.z, distance.y, distance.x ).normalized;

				Vector3 pointA = targetPosition + ( Vector3.Scale( ortho * capColl.radius * 0.9f, coll.transform.localScale ) );
				Vector3 pointB = targetPosition - ( Vector3.Scale( ortho * capColl.radius * 0.9f, coll.transform.localScale ) );

				Debug.DrawLine( targetPosition, pointA, Color.red );
				Debug.DrawLine( targetPosition, pointB, Color.blue );

				pointA = pointA - this.transform.position;
				pointB = pointB - this.transform.position;

				Debug.DrawLine( this.transform.position, this.transform.position + pointA, Color.red );
				Debug.DrawLine( this.transform.position, this.transform.position + pointB, Color.blue );

				float pointAAngle = Mathf.Repeat( Mathf.Atan2( pointA.z, pointA.x ), 2f * Mathf.PI ) * Mathf.Rad2Deg;
				float pointBAngle = Mathf.Repeat( Mathf.Atan2( pointB.z, pointB.x ), 2f * Mathf.PI ) * Mathf.Rad2Deg;

				RaycastHit hit;
				Ray ray = new Ray();

				//margine sinistro
				if ( ( pointAAngle > fovAngleRight && pointAAngle < fovAngleLeft ) ||
					 ( pointAAngle < 90f && pointAAngle < fovAngleLeft ) )
				{
					ray.origin = this.transform.position;
					ray.direction = new Vector3( pointA.x, pointA.y - 0.1f, pointA.z ).normalized; // TODO: Check 0.1f
					Debug.DrawRay( ray.origin, ray.direction * this.collider.radius, Color.green, this.RaycastPeriod );

					if ( Physics.Raycast( ray, out hit, this.collider.radius ) )
					{
						if ( ( this.TakenIntoAccount & 1 << hit.transform.gameObject.layer ) != 0 )
						{
							this.PlayerSighted( coll.GetComponent<Actor>() );
						}
					}
				}
				else
				{
					ray.origin = this.transform.position;
					ray.direction = fovPointLeft;
					Debug.DrawRay( ray.origin, ray.direction * this.collider.radius, Color.green, this.RaycastPeriod );

					if ( Physics.Raycast( ray, out hit, this.collider.radius ) )
					{
						if ( ( this.TakenIntoAccount & 1 << hit.transform.gameObject.layer ) != 0 )
						{
							this.PlayerSighted( coll.GetComponent<Actor>() );
						}
					}
				}

				//margine destro
				if ( ( pointBAngle > fovAngleRight && pointBAngle < fovAngleLeft ) ||
					 ( pointBAngle > 270f && pointBAngle > fovAngleLeft ) ) //TODO: Questa condizione è sbagliata!
				{
					ray.origin = this.transform.position;
					ray.direction = new Vector3( pointB.x, pointB.y - 0.1f, pointB.z ).normalized; // TODO: Check 0.1f
					Debug.DrawRay( ray.origin, ray.direction * this.collider.radius, Color.green, this.RaycastPeriod );

					if ( Physics.Raycast( ray, out hit, this.collider.radius ) )
					{
						if ( ( this.TakenIntoAccount & 1 << hit.transform.gameObject.layer ) != 0 )
						{
							this.PlayerSighted( coll.GetComponent<Actor>() );
						}
					}
				}
				else
				{
					ray.origin = this.transform.position;
					ray.direction = fovPointRight;
					Debug.DrawRay( ray.origin, ray.direction * this.collider.radius, Color.green, this.RaycastPeriod );

					if ( Physics.Raycast( ray, out hit, this.collider.radius ) )
					{
						if ( ( this.TakenIntoAccount & 1 << hit.transform.gameObject.layer ) != 0 )
						{
							this.PlayerSighted( coll.GetComponent<Actor>() );
						}
					}
				}
			}
		}
	}

	public void PlayerSighted( Actor player )
	{
		//Debug.Log( this.gameObject.transform.root.name + " saw the player");
		this.playerWasSeen = true;
		this.aiController.ActorSighted( player );
	}

	public void OnTriggerExit( Collider coll )
	{
		if ( this.playerWasSeen && ( this.TakenIntoAccount & 1 << coll.gameObject.layer ) != 0 )
		{
			Debug.Log( "OnTriggerExit" );
			this.playerWasSeen = false;
			this.aiController.ActorEscaped();
		}
	}
}