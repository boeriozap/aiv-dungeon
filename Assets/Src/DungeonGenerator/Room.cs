﻿
using System.Collections;
using System.Collections.Generic;
using System;

namespace AIV.Algorithms.MazeGeneration
{
    public class Room
    {
        private static System.Random s_hRand;

        static Room()
        {
            s_hRand = new System.Random();
        }


        public Tile[,] Tiles { get; private set; }

        public int X { get; private set; }
        public int Y { get; private set; }

        public List<Tile> Walls { get; private set; }

        public List<Tile> Doors { get; private set; }

        public List<Room> ConnectedRooms { get; private set; }

        public Room(int iWidth, int iHeight, int iPosX, int iPosY, Tile[,] hBoard)
        {
            for (int i = 0; i < iWidth; i++)
            {
                for (int j = 0; j < iHeight; j++)
                {
                    if (hBoard[iPosX + i, iPosY + j].Type != ContentType.Filled)
                        throw new Exception("Not Enough Space for Room");
                }
            }

            Tiles = new Tile[iWidth, iHeight];
            this.X = iPosX;
            this.Y = iPosY;
            Walls = new List<Tile>();
            Doors = new List<Tile>();
            ConnectedRooms = new List<Room>();

            for (int i = 0; i < iWidth; i++)
            {
                for (int j = 0; j < iHeight; j++)
                {
                    Tiles[i, j] = hBoard[iPosX + i, iPosY + j];
                    Tiles[i, j].Type = ContentType.Floor;
                    Tiles[i, j].Room = this;
                }
            }
        }

        internal Tile FindWall(List<Room> hRooms, System.Random hRand, out Room hConnected)
        {
            hConnected = null;

            Walls.Shuffle(hRand);

            List<Tile> hNearby = Walls[0].FindNeighbour(ContentType.Corridor);

            if (hNearby.Count > 0)
            {
                return Walls[0];
            }
            else
            {
                hNearby = Walls[0].FindNeighbour(ContentType.Floor);

                for (int k = 0; k < hNearby.Count; k++)
                {
                    if (hNearby[k].Room != this)
                    {
                        hConnected = hNearby[k].Room;
                        break;
                    }
                }

                return Walls[0];
            }

        }
    }
}