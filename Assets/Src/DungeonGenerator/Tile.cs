﻿
using System.Collections;
using System.Collections.Generic;

namespace AIV.Algorithms.MazeGeneration
{
    public class Tile
    {
        public ContentType Type { get; internal set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public Tile[] Neighbours { get; private set; }
        public Room Room { get; internal set; }


        public Tile(ContentType eType, int i, int j)
        {
            Type = eType;
            X = i;
            Y = j;
            Neighbours = new Tile[4];

        }

        internal void SetNeighbours(Tile[,] hBoard)
        {
            if (this.X + 1 < hBoard.GetLength(0))
            {
                Neighbours[(int)Direction.East] = hBoard[this.X + 1, this.Y];
                hBoard[this.X + 1, this.Y].Neighbours[(int)Direction.West] = this;
            }

            if (this.Y + 1 < hBoard.GetLength(1))
            {
                Neighbours[(int)Direction.South] = hBoard[this.X, this.Y + 1];
                hBoard[this.X, this.Y + 1].Neighbours[(int)Direction.North] = this;
            }
        }

        internal bool IsDiggable
        {
            get
            {
                //First of all the tile have to be empty
                if (this.Type != ContentType.Filled)
                    return false;

                //We dont want to open holes outside the map borders
                if (this.Neighbours[0] == null || this.Neighbours[1] == null || this.Neighbours[2] == null || this.Neighbours[3] == null)
                    return false;

                //We dont want to make tight turns, at last 1 tile have to be free
                int iCorridorNearby = 0;
                for (int i = 0; i < Neighbours.Length && iCorridorNearby < 2; i++)
                {
                    if (Neighbours[i].Type != ContentType.Filled)
                        iCorridorNearby++;
                }

                if (iCorridorNearby == 2)
                    return false;

                //bool bUpperRightBusy    = this.Neighbours[0].Neighbours[2] != null && this.Neighbours[0].Neighbours[2].Type != ContentType.Filled;
                //bool bUpperLeftBusy     = this.Neighbours[0].Neighbours[3] != null && this.Neighbours[0].Neighbours[3].Type != ContentType.Filled;
                //bool bBottomRightBusy   = this.Neighbours[1].Neighbours[2] != null && this.Neighbours[1].Neighbours[2].Type != ContentType.Filled;
                //bool bBottomLeftBusy    = this.Neighbours[1].Neighbours[3] != null && this.Neighbours[1].Neighbours[3].Type != ContentType.Filled;
                //bool bDiagonalByusy     = bUpperRightBusy || bUpperLeftBusy || bBottomRightBusy || bBottomLeftBusy;

                //if (bDiagonalByusy)
                //    return false;


                return true;
            }
        }

        internal int NearbyCount(ContentType eContentType)
        {
            int iCount = 0;

            for (int i = 0; i < this.Neighbours.Length; i++)
            {
                if (this.Neighbours[i] != null && (int)(this.Neighbours[i].Type & eContentType) != 0)
                    iCount++;
            }

            return iCount;
        }

        internal List<Tile> FindNeighbour(ContentType eType)
        {
            List<Tile> hTiles = new List<Tile>();

            for (int i = 0; i < this.Neighbours.Length; i++)
            {
                if (this.Neighbours[i] != null && (int)(this.Neighbours[i].Type & eType) != 0)
                {
                    hTiles.Add(this.Neighbours[i]);
                }
            }

            return hTiles;
        }

        /// <summary>
        /// Returns true if a Tile is on the edge of the grid
        /// </summary>
        public bool IsBorder
        {
            get
            {
                for (int i = 0; i < this.Neighbours.Length; i++)
                {
                    if (this.Neighbours[i] == null)
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true if a tile have 3 sides
        /// </summary>
        public bool IsDeadEnd
        {
            get
            {
                return (this.Type == ContentType.Corridor && this.NearbyCount(ContentType.Filled | ContentType.Wall) >= 3);
            }
        }


        public override string ToString()
        {
            return string.Format("Tile: {0}", Type.ToString());
        }
    }
}