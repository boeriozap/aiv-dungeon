﻿
using System.Collections;
using System;
using System.Collections.Generic;

namespace AIV.Algorithms.MazeGeneration
{
    [Flags]
    public enum ContentType : int
    {
        Filled = 1,    //0000 0000 0000 0001
        Floor = 2,    //0000 0000 0000 0010
        Wall = 4,    //0000 0000 0000 0100
        Door = 8,    //0000 0000 0000 1000
        Corridor = 16,   //0000 0000 0001 0000
    }

    //Utilizzo delle bitflags
    //1) Impostare l'attributo Flags sull'enum
    //2) Valorizzare gli elementi dell'enumerazione a potenza di 2
    //3) Si possono combinare i bit utilizzando l'operatore "|" vedi ContentType.Corridor | ContentType.Door
    //4) per la comparazione usare l'operatore & (int)(this.Neighbours[i].Type & eType) != 0


    public static class Extensions
    {
        public static void Shuffle<T>(this IList<T> list, System.Random hRand)
        {

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = hRand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}