﻿
using System.Collections;

namespace AIV.Algorithms.MazeGeneration
{
    public enum Direction : byte
    {
        North = 0,
        South = 1,
        East = 2,
        West = 3
    }
}