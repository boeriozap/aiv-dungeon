﻿
using System.Collections;
using System.Collections.Generic;
using System;

namespace AIV.Algorithms.MazeGeneration
{	
    public class Dungeon
    {
        public Tile[,] Tiles { get; private set; }

        public List<Room> Rooms { get; private set; }

        private System.Random m_hRand;

        private const int MaxErrorTries = 80;

        public int Seed { get; private set; }
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public int RoomCount { get; private set; }
        public int RoomMinSize { get; private set; }
        public int RoomMaxSize { get; private set; }
        public bool DeadEnds { get; private set; }


        public Dungeon(int iSeed, int iRows, int iColumns, int iRoomCount, int iRoomMinSize, int iRoomMaxSize, bool bRemoveDeadEnds)
        {
            m_hRand = new System.Random(iSeed);
            Seed = iSeed;
            Rows = iRows;
            Columns = iColumns;
            RoomCount = iRoomCount;
            RoomMinSize = iRoomMinSize;
            RoomMaxSize = iRoomMaxSize;
            DeadEnds = bRemoveDeadEnds;

            Tiles = new Tile[iRows, iColumns];
            Rooms = new List<Room>();

            //Initialize the tiles by fillin up
            for (int i = 0; i < iRows; i++)
            {
                for (int j = 0; j < iColumns; j++)
                {
                    Tiles[i, j] = new Tile(ContentType.Filled, i, j);
                }
            }

            //Connect Tiles each other
            for (int i = 0; i < iRows; i++)
            {
                for (int j = 0; j < iColumns; j++)
                {
                    Tiles[i, j].SetNeighbours(Tiles);
                }
            }


            //Dig Rooms        
            int iSubSeqErrorCount = 0;
            for (int i = 0; i < iRoomCount && iSubSeqErrorCount < MaxErrorTries; i++)
            {
                int iWidth = m_hRand.Next(iRoomMinSize, iRoomMaxSize);
                int iHeight = m_hRand.Next(iRoomMinSize, iRoomMaxSize);
                int iPosX = m_hRand.Next(0, iRows);
                int iPosY = m_hRand.Next(0, iColumns);

                if (iPosX % 2 == 0 || iPosY % 2 == 0 || iWidth % 2 == 0 || iHeight % 2 == 0)
                {
                    i--;
                    continue;
                }

                try
                {
                    Rooms.Add(new Room(iWidth, iHeight, iPosX, iPosY, Tiles));
                    iSubSeqErrorCount = 0;
                }
                catch (Exception)
                {
                    iSubSeqErrorCount++;
                    i--;
                }
            }

            //Generate Maze
            Stack<Tile> hToVisit = new Stack<Tile>();
            List<Tile> hToShuffle = new List<Tile>();
            hToVisit.Push(this.FindBuildPlace());

            while (true)
            {
                while (hToVisit.Count > 0)
                {
                    Tile hCurrent = hToVisit.Pop();

                    if (hCurrent.IsDiggable)
                    {
                        hCurrent.Type = ContentType.Corridor;

                        if (hCurrent.X % 2 != 0 && hCurrent.Y % 2 != 0)
                        {
                            //i can go vertical or horzontal
                            hToShuffle.AddRange(hCurrent.Neighbours);
                        }
                        else if (hCurrent.X % 2 == 0 && hCurrent.Y % 2 != 0)
                        {
                            //posso andare solo a dx e sx                        
                            hToShuffle.Add(hCurrent.Neighbours[2]);
                            hToShuffle.Add(hCurrent.Neighbours[3]);
                        }
                        else if (hCurrent.X % 2 != 0 && hCurrent.Y % 2 == 0)
                        {
                            //posso andare solo alto e basso
                            hToShuffle.Add(hCurrent.Neighbours[0]);
                            hToShuffle.Add(hCurrent.Neighbours[1]);
                        }
                        else
                        {
                            //Do not insert
                        }


                        //Insert into stack
                        hToShuffle.Shuffle(m_hRand);

                        for (int i = 0; i < hToShuffle.Count; i++)
                        {
                            if (hToShuffle[i].Type == ContentType.Filled)
                            {
                                hToVisit.Push(hToShuffle[i]);
                            }
                        }

                        hToShuffle.Clear();
                    }
                }

                Tile hNewPos = this.FindBuildPlace();
                if (hNewPos != null)
                    hToVisit.Push(hNewPos);
                else
                    break;
            }

            //Todo: Flag Walls
            for (int i = 0; i < Tiles.GetLength(0); i++)
            {
                for (int j = 0; j < Tiles.GetLength(1); j++)
                {
                    Tile hCurrent = Tiles[i, j];

                    if (hCurrent.Type != ContentType.Filled || hCurrent.IsBorder)
                        continue;

                    int iFloors = hCurrent.NearbyCount(ContentType.Floor);
                    int iCorridor = hCurrent.NearbyCount(ContentType.Corridor);

                    //Se ha almeno una stanza vicino è candidato
                    if (iFloors > 0 && (iCorridor == 1 || iFloors == 2))
                    {
                        List<Tile> hTiles = hCurrent.FindNeighbour(ContentType.Floor);

                        hCurrent.Type = ContentType.Wall; //Flagghiamo di verde x debug

                        hTiles.ForEach(hT =>
                        {
                            hT.Room.Walls.Add(hCurrent);
                        });
                    }
                }
            }

            //Open Doors        
            List<Room> hRoomsToOpen = new List<Room>(this.Rooms);

            while (hRoomsToOpen.Count > 0)
            {
                Room hCurrent = hRoomsToOpen[0];
                hRoomsToOpen.RemoveAt(0);

                this.OpenDoor(hCurrent, hRoomsToOpen);
            }


            //Remove Deadends
            if (!bRemoveDeadEnds)
                return;

            List<Tile> hDeadEnds = this.FindDeadEnds();

            for (int i = 0; i < hDeadEnds.Count; i++)
            {
                Tile hDeadEnd = hDeadEnds[i];

                while (hDeadEnd != null)
                {
                    List<Tile> hPrevious = hDeadEnd.FindNeighbour(ContentType.Corridor | ContentType.Door);

                    if (hPrevious.Count == 0)
                    {
                        hDeadEnd.Type = ContentType.Filled;
                        hDeadEnd = null; //caso circodato da 4 filled
                    }
                    else if (hPrevious.Count == 1)
                    {
                        hDeadEnd.Type = hDeadEnd.Type == ContentType.Door ? ContentType.Wall : ContentType.Filled;
                        hDeadEnd = hPrevious[0];
                    }
                    else
                    {
                        hDeadEnd = null;
                    }
                }
            }
        }

        private void OpenDoor(Room hCurrent, List<Room> hRoomsToOpen)
        {
            Room hOutRoom;

            //Prendi un muro a caso
            Tile hTile = hCurrent.FindWall(hRoomsToOpen, m_hRand, out hOutRoom);

            //Trasformalo in porta
            hTile.Type = ContentType.Door;

            //Se apre su un corridoio  ho finito
            if (hOutRoom == null)
            {
                hRoomsToOpen.Remove(hOutRoom);
            }
            else
            {
                this.OpenDoor(hOutRoom, hRoomsToOpen);
            }
        }



        private Tile FindBuildPlace()
        {
            for (int i = 1; i < Tiles.GetLength(0); i += 2)
            {
                for (int j = 1; j < Tiles.GetLength(1); j += 2)
                {
                    Tile hCurrent = Tiles[i, j];

                    bool bNordValid = hCurrent.Neighbours[0] != null && hCurrent.Neighbours[0].Type == ContentType.Filled;
                    bool bSouthValid = hCurrent.Neighbours[1] != null && hCurrent.Neighbours[1].Type == ContentType.Filled;
                    bool bEastValid = hCurrent.Neighbours[2] != null && hCurrent.Neighbours[2].Type == ContentType.Filled;
                    bool bWestValid = hCurrent.Neighbours[3] != null && hCurrent.Neighbours[3].Type == ContentType.Filled;

                    if (bNordValid && bSouthValid && bEastValid && bWestValid && hCurrent.Type == ContentType.Filled)
                        return hCurrent;
                }
            }

            return null;
        }

        private List<Tile> FindDeadEnds()
        {
            List<Tile> hResult = new List<Tile>();

            for (int i = 0; i < Tiles.GetLength(0); i++)
            {
                for (int j = 0; j < Tiles.GetLength(1); j++)
                {
                    if (Tiles[i, j].IsDeadEnd)
                        hResult.Add(Tiles[i, j]);
                }
            }

            return hResult;
        }
    }



}