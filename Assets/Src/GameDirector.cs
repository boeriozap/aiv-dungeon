﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
	public static GameDirector Instance { get; private set; }

	public List<DifficultyLevel> Difficulties;

	public DifficultyLevel CurrentSettings { get; set; }

	public Object GuardPrefab;

	public GameObject GUICanvas;

	public new Camera3rdPerson camera { get; private set; }

	public UIInteractionPanel UIInteractionPanel { get; private set; }

	public float ScoreLossPerSecond = 0f;
	public int LootScore = 0;
	public float TimeScore = 0;
	public int TotalScore = 0;

	private List<GameObject> waypoints = new List<GameObject>();

	private void Awake()
	{
		if ( Instance != null )
			throw new System.Exception( "Duplicated GameDirector Detected!" );

		Instance = this;
		GameObject.DontDestroyOnLoad( this );

#if UNITY_EDITOR
		if ( Difficulties.Count == 0 )
			throw new System.Exception( "No Difficulty Settings Defined, please edit GameDirector" );

		CurrentSettings = Difficulties[0];
#endif

		if ( GUICanvas == null )
			throw new System.Exception( "Missing GUI Canvas" );

		this.UIInteractionPanel = this.GUICanvas.GetComponentInChildren<UIInteractionPanel>();
		if ( this.UIInteractionPanel == null )
			throw new System.Exception( "Missing UIInteractionPanel" );

		this.UIInteractionPanel.gameObject.SetActive( false );

		this.camera = GameObject.FindObjectOfType<Camera3rdPerson>();
	}

	private void Start()
	{
		System.Random hRand = new System.Random();
		//Otteniamo i punti di spawn delle guardie

		this.waypoints = GameObject.FindGameObjectsWithTag( "Floor" ).ToList();

		int iGuardNumber = Random.Range( CurrentSettings.MinGuardsCount, CurrentSettings.MaxGuardsCount );

		List<GameObject> hPositions = new List<GameObject>( waypoints );

		for ( int i = 0; i < iGuardNumber && i < hPositions.Count; i++ )
		{
			int iIndex = Random.Range( 0, hPositions.Count );
			GameObject hSpawnPoint = hPositions[iIndex];

			GameObject hGuard = GameObject.Instantiate( GuardPrefab, hSpawnPoint.transform.position, Quaternion.Euler( 0f, Random.Range( 0f, 360f ), 0f ) ) as GameObject;
			hGuard.name = "Guard" + i;

			NavMeshAgent hAgent = hGuard.GetComponent<NavMeshAgent>();
			hAgent.avoidancePriority = i;

			AIController hController = hGuard.GetComponent<AIController>();
			hController.IdleTime *= CurrentSettings.GuardWaitTimeCoeff;

			hPositions.RemoveAt( iIndex );
		}
	}

	private void Update()
	{
		this.TimeScore += Time.deltaTime;
	}

	public void ComputeTotalScore()
	{
		this.TotalScore = this.LootScore - Mathf.FloorToInt( this.TimeScore * this.ScoreLossPerSecond );

		if ( this.TotalScore < 0 )
			this.TotalScore = 0;
	}

	[System.Serializable]
	public class DifficultyLevel
	{
		public string Name;

		public int MinGuardsCount;
		public int MaxGuardsCount;
		public float GuardWaitTimeCoeff = 1.0f;
	}

	public bool MenuIsOpen { get { return false; } } // FIXME!

	internal GameObject GetWaypoint()
	{
		//Debug.Log( m_hFloors.Count );

		if ( waypoints.Count == 0 )
			return null;
		else
		{
			return waypoints[Random.Range( 0, this.waypoints.Count )];
		}
	}
}