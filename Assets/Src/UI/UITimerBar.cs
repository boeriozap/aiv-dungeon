﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class UITimerBar : MonoBehaviour
{
	public Slider Slider { get; private set; }

	private void Awake()
	{
		this.Slider = this.GetComponent<Slider>();

		Interaction.ExecutionBegin += this.OnInteractionStart;
		Interaction.ExecutionUpdate += this.OnInteractionUpdate;
		Interaction.ExecutionEnd += this.OnInteractionFinish;
		Interaction.ExecutionCancel += this.OnInteractionCancel;

		this.gameObject.SetActive( false );
	}

	private void OnDestroy()
	{
		Interaction.ExecutionBegin -= this.OnInteractionStart;
		Interaction.ExecutionUpdate -= this.OnInteractionUpdate;
		Interaction.ExecutionEnd -= this.OnInteractionFinish;
		Interaction.ExecutionCancel -= this.OnInteractionCancel;
	}

	void OnInteractionStart( Interaction interaction )
	{
		// TODO: Do this better
		if ( interaction.Duration <= 0f ) return;

		this.gameObject.SetActive( true );
		this.Slider.minValue = 0f;
		this.Slider.maxValue = interaction.Duration;
		this.Slider.value = interaction.ExecutionTime;
	}

	void OnInteractionUpdate( Interaction interaction )
	{
		this.Slider.value = interaction.ExecutionTime;
	}

	void OnInteractionFinish( Interaction interaction )
	{
		this.Slider.value = interaction.ExecutionTime;
		this.gameObject.SetActive( false );
	}

	void OnInteractionCancel( Interaction interaction )
	{
		this.Slider.value = interaction.ExecutionTime;
		this.gameObject.SetActive( false );
	}
}
