using UnityEngine;

public class Camera3rdPerson : MonoBehaviour
{
	public Transform Target
	{
		get { return this.cameraTarget; }
		set
		{
			this.oldTargetPosition = this.cameraTarget.position;
			this.cameraTarget = value;
			this.targetChangeTimer = 0f;
		}
	}

	[SerializeField]
	private Transform cameraTarget;

	private Vector3? oldTargetPosition = null;
	private float y = .0f;
	private float x = .0f;

	private int mouseXSpeedMod = 4;
	private int mouseYSpeedMod = -2;

	public float minViewDistance = 1;
	public float maxViewDistance = 25;
	public int lerpRate = 5;

	public float TargetChangeLerpTime = 1f;
	private float targetChangeTimer = 0f;

	private int zoomRate = 30;
	private float distance = 3;
	private float desiredDistance = 0f;
	private float correctedDistance = 0f;
	private float currentDistance;

	public float cameraTargetHeight = 1.5f;
	public float heightForCollision = 2f;

	private void Start()
	{
		Vector3 angles = this.transform.eulerAngles;
		this.y = angles.x;
		this.x = angles.y;

		this.currentDistance = this.distance;
		this.desiredDistance = this.distance;
		this.correctedDistance = this.distance;
	}

	private void LateUpdate()
	{
		if ( Input.GetKey( KeyCode.Mouse1 ) )
		{
			this.y += Input.GetAxis( "Mouse X" ) * this.mouseXSpeedMod;
			this.x += Input.GetAxis( "Mouse Y" ) * this.mouseYSpeedMod;
		}
		else if ( Input.GetAxis( "Vertical" ) != 0 || Input.GetAxis( "Horizontal" ) != 0 )
		{
			float targetRotationAngle = this.cameraTarget.eulerAngles.y;
			float cameraRotationAngle = this.transform.eulerAngles.y;
			this.y = Mathf.LerpAngle( cameraRotationAngle, targetRotationAngle, this.lerpRate * Time.deltaTime );
		}

		this.x = ClampAngle( x, -50, 90 );

		Quaternion rotation = Quaternion.Euler( this.x, this.y, 0 );

		// calcolo la distanza che vuole ottenere il giocatore, con la rotella
		this.desiredDistance -= Input.GetAxis( "Mouse ScrollWheel" ) * Time.deltaTime * this.zoomRate * Mathf.Abs( this.desiredDistance );
		this.desiredDistance = Mathf.Clamp( this.desiredDistance, this.minViewDistance, this.maxViewDistance );
		this.correctedDistance = this.desiredDistance;

		Vector3 newPosition;

		if ( !this.oldTargetPosition.HasValue )
		{
			newPosition = this.cameraTarget.position - ( rotation * Vector3.forward * this.desiredDistance ); 	// (x,y,z) * (0,1,0) * (angolo in gradi)
		}
		else
		{
			newPosition = Vector3.Slerp( this.oldTargetPosition.Value, this.cameraTarget.position, this.targetChangeTimer / this.TargetChangeLerpTime ) -
				( rotation * Vector3.forward * this.desiredDistance );
		}

		RaycastHit collisionHit;
		Vector3 cameraTargetPosition = new Vector3( this.cameraTarget.position.x, this.cameraTarget.position.y + this.cameraTargetHeight, this.cameraTarget.position.z );

		// controllo che tra il target e la camera non ci siano oggetti d'intralcio, se così, avvicino la camera tenendola aderente al punto di contatto
		bool isCorrected = false;
		if ( this.transform.position.y < this.heightForCollision )
		{
			if ( Physics.Linecast( cameraTargetPosition, newPosition, out collisionHit ) )
			{
				newPosition = collisionHit.point;
				this.correctedDistance = Vector3.Distance( cameraTargetPosition, newPosition );
				isCorrected = true;
			}
		}

		this.currentDistance = !isCorrected || this.correctedDistance > this.currentDistance ?
			Mathf.Lerp( this.currentDistance, this.correctedDistance, Time.deltaTime * this.zoomRate ) :
			this.correctedDistance;

		if ( !this.oldTargetPosition.HasValue )
		{
			newPosition = this.cameraTarget.position - ( rotation * Vector3.forward * currentDistance +
				new Vector3( 0, -this.cameraTargetHeight, 0 ) );
		}
		else
		{
			newPosition = Vector3.Slerp( this.oldTargetPosition.Value, this.cameraTarget.position, this.targetChangeTimer / this.TargetChangeLerpTime ) -
			( rotation * Vector3.forward * currentDistance + new Vector3( 0, -this.cameraTargetHeight, 0 ) );

			if ( this.targetChangeTimer >= this.TargetChangeLerpTime )
			{
				this.oldTargetPosition = null;
			}

			this.targetChangeTimer += Time.deltaTime;
		}

		this.transform.rotation = rotation;
		this.transform.position = newPosition;
	}

	private static float ClampAngle( float angle, float min, float max )
	{
		if ( angle < -360 ) angle += 360;
		if ( angle > 360 ) angle -= 360;
		return Mathf.Clamp( angle, min, max );
	}
}