﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class UIScorePanel : MonoBehaviour
{
	[SerializeField]
	private Text LootValueText;
	[SerializeField]
	private Text TimeValueText;
	[SerializeField]
	private Text TotalValueText;

	private int lootValue;
	private float timeValue;
	private int totalValue;


	void OnEnable()
	{
		this.lootValue = 0;
		this.timeValue = 0f;
		this.totalValue = 0;

		DOTween.To( () => this.lootValue, x => this.lootValue = x,
				   GameDirector.Instance.LootScore, 1.5f ).SetEase( Ease.OutCubic ).OnUpdate(
				   () => this.LootValueText.text = this.lootValue.ToString() );

		DOTween.To( () => this.timeValue, x => this.timeValue = x,
				   GameDirector.Instance.TimeScore, 1.5f ).SetEase( Ease.OutCubic ).OnUpdate(
				   () => this.TimeValueText.text = this.timeValue.ToString() ).SetDelay( 1f );

		DOTween.To( () => this.totalValue, x => this.totalValue = x,
				   GameDirector.Instance.TotalScore, 1.5f ).SetEase( Ease.OutCubic ).OnUpdate(
				   () => this.TotalValueText.text = this.totalValue.ToString() ).SetDelay( 2f );
	}
}
