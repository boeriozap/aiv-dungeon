﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

//public enum UIInteractionButton
//{
//	Open = 1 << 0,
//	Close = 1 << 1,
//	Peek = 1 << 2,
//	Lock = 1 << 3,
//	Unlock = 1 << 4,
//	Hide = 1 << 5,
//	Loot = 1 << 6
//}

public class UIInteractionPanel : MonoBehaviour
{
	public event Action Opened;

	public event Action Closed;

	public bool IsOpen { get { return this.isActiveAndEnabled; } }

	private Interaction[] interactions;

	private Button[] buttons;
	private RectTransform rectTransform;

	private void Awake()
	{
		this.rectTransform = this.transform as RectTransform;
		this.buttons = this.transform.GetComponentsInChildren<Button>();
		for ( int i = 0; i < this.buttons.Length; i++ )
		{
			this.buttons[i].gameObject.SetActive( false );
		}
	}

	private void Update()
	{
		if ( Input.GetKeyUp( KeyCode.Mouse0 ) || Input.GetKeyUp( KeyCode.Mouse1 ) )
		{
			if ( !RectTransformUtility.RectangleContainsScreenPoint( this.rectTransform, Input.mousePosition, null ) )
			{
				this.Close();
			}
		}
	}

	public void Open( InteractableObject target, params Interaction[] interactions )
	{
		this.transform.position = Input.mousePosition;
		this.gameObject.SetActive( true );
		this.enabled = true;
		this.SetButtonsEnabled( interactions.Select(i => i.ActionName).ToArray() );

		this.interactions = interactions;

		if ( this.Opened != null )
			this.Opened();
	}

	public void Close()
	{
		this.gameObject.SetActive( false );
		this.interactions = null;

		if ( this.Closed != null )
			this.Closed();
	}

	private void SetButtonsEnabled( params string[] enabledButtons )
	{
		for ( int i = 0; i < this.buttons.Length; i++ )
		{
			if ( i < enabledButtons.Length )
			{
				this.buttons[i].gameObject.SetActive( true );
				this.buttons[i].GetComponentInChildren<Text>().text = enabledButtons[i];
			}
			else
			{
				this.buttons[i].gameObject.SetActive( false );
			}
		}
	}

	public void OnButtonClick( Button clicked )
	{
		int buttonIndex = Array.IndexOf( this.buttons, clicked );
		this.interactions[buttonIndex].Begin();
		PlayerController.Instance.CurrentInteraction = this.interactions[buttonIndex];
		this.Close();
	}
}