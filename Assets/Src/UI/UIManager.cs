﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
	public static UIManager Instance { get; private set; }

	public UILootValuePanel LootValuePanel;
	public UIScorePanel ScorePanel;
	

	private void Awake()
	{
		if ( Instance != null && Instance != this ) 
			throw new System.Exception( "Duplicate UIManager" );

		Instance = this;
	}

	private void OnDestroy()
	{
		Instance = null;
	}
}