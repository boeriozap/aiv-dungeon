﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UILootValuePanel : MonoBehaviour
{
	[SerializeField]
	private Text valueText;

	private int lootValue;
	private int targetLootValue;
	private object lootValueTweenID;

	private void Update()
	{
		if ( GameDirector.Instance.LootScore != this.targetLootValue )
		{
			this.targetLootValue = GameDirector.Instance.LootScore;

			DOTween.Kill( this.lootValueTweenID );

			var tween = DOTween.To( () => this.lootValue,
				x => this.lootValue = x,
				this.targetLootValue, 1f ).SetEase( Ease.OutCubic ).OnUpdate(
				() => this.valueText.text = this.lootValue.ToString() );

			this.lootValueTweenID = tween.id;

			DOTween.Play( this.lootValueTweenID );
		}
	}
}