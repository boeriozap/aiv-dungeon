﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( InteractableObject ) )]
public abstract class Interaction : MonoBehaviour
{
	public delegate void ExecutionUpdateHandler( Interaction interaction );

	public static event ExecutionUpdateHandler ExecutionBegin;
	public static event ExecutionUpdateHandler ExecutionUpdate;
	public static event ExecutionUpdateHandler ExecutionEnd;
	public static event ExecutionUpdateHandler ExecutionCancel;


	public float Duration = 1f;
	public float ActorMaxDistance = 1f;
	public bool IsCancellable = true;
	public bool ResetsOnCancel = true;

	public abstract string ActionName { get; }
	public abstract bool IsExecutable { get; }
	public float ExecutionTime { get; private set; }
	public bool IsExecuting { get { return this.InteractableObject.CurrentInteraction == this; } }

	public InteractableObject InteractableObject { get; private set; }

	/// <summary> MUST BE CALLED BY DERIVED CLASSES </summary>
	protected virtual void Awake()
	{
		this.InteractableObject = this.GetComponent<InteractableObject>();
	}


	public virtual void Begin()
	{
		if ( !this.IsExecuting )
		{
			if ( this.InteractableObject.BeginInteractionInternal( this ) )
			{
				if ( Interaction.ExecutionBegin != null )
				{
					Interaction.ExecutionBegin( this );
				}
			}
		}
	}

	public virtual void Cancel()
	{
		if ( this.IsExecuting )
		{
			if ( this.InteractableObject.EndInteractionInternal( this ) )
			{
				if ( Interaction.ExecutionCancel != null )
				{
					Interaction.ExecutionCancel( this );
				}

				if ( this.ResetsOnCancel )
				{
					this.ExecutionTime = 0f;
				}
			}
		}
	}

	protected virtual void Update()
	{
		if ( this.InteractableObject == null )
		{
			Debug.LogError( "Missing InteractableObject on " + this.name );
			return;
		}

		if ( this.IsExecuting )
		{
			this.ExecutionTime += Time.deltaTime;
			this.ExecutionTime = Mathf.Clamp( this.ExecutionTime, 0f, this.Duration );

			if ( Interaction.ExecutionUpdate != null )
			{
				Interaction.ExecutionUpdate( this );
			}

			if ( Mathf.Approximately( this.ExecutionTime, this.Duration ) )
			{
				this.End();
			}
		}
	}

	protected virtual void End()
	{
		if ( this.IsExecuting )
		{
			if ( this.InteractableObject.EndInteractionInternal( this ) )
			{
				if ( Interaction.ExecutionEnd != null )
				{
					Interaction.ExecutionEnd( this );
				}

				this.ExecutionTime = 0f;
			}
		}
	}
}