﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableObject : MonoBehaviour, IPointerClickHandler
{
	public Interaction CurrentInteraction { get; private set; }
	public Actor LastInteractor { get; private set; }

	private Interaction[] interactions;

	public Interaction[] Interactions
	{
		get { return this.interactions; }
		set { this.interactions = value; }
	}

	public Transform InteractionPoint;

	public bool IsExecutingInteraction
	{
		get { return this.CurrentInteraction != null; }
	}

	private void Awake()
	{
		this.interactions = this.GetComponents<Interaction>();
	}

	/// <summary> YOU SHALL NOT CALL THIS FUNCTION EVER. Use Interaction.Begin() instead </summary>
	public bool BeginInteractionInternal( Interaction interaction )
	{
		if ( this.CurrentInteraction != null || !this.interactions.Contains( interaction ) )
			return false;
		this.CurrentInteraction = interaction;
		return true;
	}

	/// <summary> YOU SHALL NOT CALL THIS FUNCTION EVER. Use Interaction.End() instead </summary>
	public bool EndInteractionInternal( Interaction interaction )
	{
		if ( this.CurrentInteraction != interaction )
			return false;
		this.CurrentInteraction = null;
		return true;
	}

	public void ShowInteractions( Actor interactor )
	{
		this.LastInteractor = interactor;
		float distance = float.MaxValue;
		if ( this.InteractionPoint != null )
		{
			distance = Vector2.Distance( interactor.transform.position.XZ(), this.InteractionPoint.position.XZ() );
		}
		else
		{
			distance = Vector2.Distance( interactor.transform.position.XZ(), this.transform.position.XZ() );
		}

		List<Interaction> inRange = this.interactions.Where( action => action.IsExecutable &&
																	   distance <= action.ActorMaxDistance ).ToList();

		if ( inRange.Count > 0 )
		{
			GameDirector.Instance.UIInteractionPanel.Open( this, inRange.ToArray() );
		}
	}

	void IPointerClickHandler.OnPointerClick( PointerEventData eventData )
	{
		if ( eventData.button == PointerEventData.InputButton.Right )
		{
			this.ShowInteractions( PlayerController.Instance );
		}
	}
}