﻿public class DoorPickLockInteraction : Interaction
{
	public Door Door { get; private set; }
	public float NoiseIntensity = 1f;

	protected override void Awake()
	{
		base.Awake();
		this.Door = this.GetComponentInParent<Door>();
	}

	public override string ActionName
	{
		get { return "Pick lock"; }
	}

	public override bool IsExecutable
	{
		get { return this.Door.IsLocked; }
	}

	public override void Begin()
	{
		base.Begin();

		AudioManager.Instance.Play( SoundClip.Lockpicking, this.transform.position );
	}

	protected override void Update()
	{
		base.Update();

		if ( this.IsExecuting && this.InteractableObject.LastInteractor != null )
			this.InteractableObject.LastInteractor.MakeNoise( SoundClip.Lockpicking, this.NoiseIntensity );
	}

	protected override void End()
	{
		base.End();

		if ( this.Door.OpenedSide == Door.DoorState.Closed )
		{
			this.Door.IsLocked = false;
		}
	}
}