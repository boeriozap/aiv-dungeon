﻿using UnityEngine;

public class LootInteraction : Interaction
{
	public SoundClip SoundClip = SoundClip.None;
	public int Value = 1;

	public override string ActionName { get { return "Loot"; } }

	public override bool IsExecutable { get { return true; } }

	protected override void End()
	{
		base.End();

		GameDirector.Instance.LootScore += this.Value;
		AudioManager.Instance.Play( this.SoundClip, this.transform.position );

		Object.Destroy( this.gameObject );
	}

}