﻿using UnityEngine;
using System.Collections;

public class DoorOpenInteraction : Interaction
{
	public Door Door { get; private set; }

	protected override void Awake()
	{
		base.Awake();
		this.Door = this.GetComponentInParent<Door>();
	}

	public override bool IsExecutable
	{
		get
		{
			return this.Door.OpenedSide != Door.DoorState.Closed ||
				   ( this.Door.OpenedSide == Door.DoorState.Closed && !this.Door.IsLocked );
		}
	}

	public override string ActionName
	{
		get { return this.Door.OpenedSide == Door.DoorState.Closed ? "Open" : "Close"; }
	}

	protected override void End()
	{
		base.End();

		if ( this.Door.OpenedSide == Door.DoorState.Closed )
		{
			this.Door.Open( this.InteractableObject.LastInteractor.transform.position );
		}
		else
		{
			this.Door.Close();
		}
	}
}