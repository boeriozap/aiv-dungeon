﻿using UnityEngine;

public class HidingInteraction : Interaction
{
	public float NoiseIntensity = 1f;

	private bool justHidden;
	private static int? weaponsLayer;

	public override string ActionName
	{
		get { return "Hide"; }
	}

	public override bool IsExecutable
	{
		get { return true; }
	}

	protected override void Awake()
	{
		base.Awake();

		if ( !HidingInteraction.weaponsLayer.HasValue )
		{
			HidingInteraction.weaponsLayer = LayerMask.NameToLayer( "Weapons" );
		}
	}

	protected override void Update()
	{
		base.Update();

		// TODO: Remove UI dependency
		// TODO: Stopping hiding should be handled as a separate action

		if ( PlayerController.Instance.HidingPlace == this.gameObject )
		{
			if ( Input.GetKeyUp( KeyCode.Mouse0 ) && !this.justHidden )
			{
				this.StopHiding();
			}

			this.justHidden = false;
		}
	}

	protected override void End()
	{
		base.End();

		PlayerController.Instance.Hide( this.gameObject );
		GameDirector.Instance.camera.Target = this.transform;
		this.justHidden = true;
	}

	private void StopHiding()
	{
		PlayerController.Instance.Hide( null );
		GameDirector.Instance.camera.Target = PlayerController.Instance.transform;

		if ( this.InteractableObject.LastInteractor != null )
			this.InteractableObject.LastInteractor.MakeNoise( SoundClip.None,  this.NoiseIntensity );
	}

	private void OnTriggerEnter( Collider other )
	{
		if ( other.gameObject.layer == HidingInteraction.weaponsLayer &&
			 PlayerController.Instance.HidingPlace == this.gameObject )
		{
			var weapon = other.GetComponent<Weapon>();

			PlayerController.Instance.Health -= weapon.Damage;
		}
	}
}