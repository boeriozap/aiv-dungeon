﻿using UnityEngine;

public class LevelExitInteraction : Interaction
{
	public override string ActionName { get { return "Exit"; } }

	public override bool IsExecutable { get { return true; } }

	protected override void End()
	{
		base.End();

		GameDirector.Instance.ComputeTotalScore();
		// TODO: NON DIPENDERE DALL'INTERFACCIA!!!!!!!!
		UIManager.Instance.ScorePanel.gameObject.SetActive( true );
		Debug.Log( "Exit level" );
	}
}