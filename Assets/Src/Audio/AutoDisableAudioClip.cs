﻿using UnityEngine;

public class AutoDisableAudioClip : MonoBehaviour
{
	public AudioSource Source;

	void Update()
	{
		if ( !this.Source.isPlaying )
			Object.Destroy( this.gameObject );
	}
}