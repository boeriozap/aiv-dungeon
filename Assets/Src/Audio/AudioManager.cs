﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundClip
{
	None,
	Lockpicking,
	DoorOpening,
	DoorClosing,
	LootingCoins,
	Walking,
	Running,
}

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance { get; private set; }

	private readonly List<AutoDisableAudioClip> soundClips = new List<AutoDisableAudioClip>();

	public AutoDisableAudioClip Play( SoundClip soundClip, Vector3 position )
	{
		var soundClipIndex = (int) soundClip - 1;
		if ( soundClipIndex >= 0 && this.soundClips.Count > soundClipIndex )
		{
			var clip = Object.Instantiate( this.soundClips[soundClipIndex] );
			clip.transform.position = position;
			clip.Source.Play();

			return clip;
		}

		return null;
	}

	private void Awake()
	{
		//TODO: Full check Instance
		AudioManager.Instance = this;

		this.soundClips.Add( Resources.Load<AutoDisableAudioClip>( "Audio/LockPicking" ) );
		this.soundClips.Add( Resources.Load<AutoDisableAudioClip>( "Audio/DoorOpening" ) );
		this.soundClips.Add( Resources.Load<AutoDisableAudioClip>( "Audio/DoorClosing" ) );
		this.soundClips.Add( Resources.Load<AutoDisableAudioClip>( "Audio/LootingCoins" ) );
	}
}